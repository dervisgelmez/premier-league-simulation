<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameStatisticTable extends Migration
{
    const types = ['possession', 'goal', 'shot', 'shot_on_target', 'pass', 'pass_on_target', 'corner', 'offside', 'foul', 'yellow_card', 'red_card'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_statistic', function (Blueprint $table) {
            $table->id();
            $table->enum('type', self::types);
            $table->tinyInteger('home_team_value')->nullable();
            $table->tinyInteger('away_team_value')->nullable();

            /* Relationships */
            $table->bigInteger('game_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_statistic');
    }
}
