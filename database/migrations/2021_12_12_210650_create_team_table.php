<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('attack');
            $table->string('middle');
            $table->string('defense');
            $table->string('overall');
            $table->tinyInteger('goals_scored')->nullable();
            $table->tinyInteger('goal_conceded')->nullable();
            $table->tinyInteger('win')->nullable();
            $table->tinyInteger('draw')->nullable();
            $table->tinyInteger('lose')->nullable();
            $table->tinyInteger('average')->nullable(0);
            $table->tinyInteger('point')->nullable(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team');
    }
}
