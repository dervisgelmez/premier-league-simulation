<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameMomentTable extends Migration
{
    const types = ['goal', 'assist', 'foul', 'player_change', 'starting_game'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_moment', function (Blueprint $table) {
            $table->id();
            $table->enum('type', self::types);
            $table->char('minute', 3);
            $table->string('data');

            /* Relationships */
            $table->bigInteger('game_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_moment');
    }
}
