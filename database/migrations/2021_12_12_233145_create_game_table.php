<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game', function (Blueprint $table) {
            $table->id();
            $table->char('week_number', 2);
            $table->boolean('completed')->nullable();
            $table->dateTime('match_date');

            /* Team statistic */
            $table->tinyInteger('home_team_goals')->nullable();
            $table->tinyInteger('away_team_goals')->nullable();

            /* Relationships */
            $table->bigInteger('fixture_id');
            $table->bigInteger('home_team_id');
            $table->bigInteger('away_team_id');
            $table->bigInteger('referee_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game');
    }
}
