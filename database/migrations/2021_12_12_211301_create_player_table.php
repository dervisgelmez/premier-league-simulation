<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->char('number', 2)->nullable();
            $table->string('position')->nullable();
            $table->char('age', 2)->nullable();
            $table->string('foot')->nullable();
            $table->boolean('squad')->nullable();

            /* Player statistics */
            $table->tinyInteger('goal')->nullable();
            $table->tinyInteger('assist')->nullable();
            $table->tinyInteger('red_card')->nullable();
            $table->tinyInteger('yellow_card')->nullable();
            $table->tinyInteger('injury_week')->nullable();
            $table->tinyInteger('out_of_play')->nullable();

            /* Player attributes */
            $table->tinyInteger('overall')->nullable();
            $table->tinyInteger('ball_control_attr')->nullable();
            $table->tinyInteger('dribbling_attr')->nullable();
            $table->tinyInteger('crossing_pass_attr')->nullable();
            $table->tinyInteger('short_pass_attr')->nullable();
            $table->tinyInteger('long_pass_attr')->nullable();
            $table->tinyInteger('power_shot_attr')->nullable();
            $table->tinyInteger('finishing_attr')->nullable();
            $table->tinyInteger('long_shot_attr')->nullable();
            $table->tinyInteger('head_shot_attr')->nullable();
            $table->tinyInteger('curve_shoot_attr')->nullable();
            $table->tinyInteger('free_kick_attr')->nullable();
            $table->tinyInteger('penalty_attr')->nullable();
            $table->tinyInteger('slide_tackle_attr')->nullable();
            $table->tinyInteger('stand_tackle_attr')->nullable();
            $table->tinyInteger('marking_attr')->nullable();
            $table->tinyInteger('acceleration_attr')->nullable();
            $table->tinyInteger('strength_attr')->nullable();
            $table->tinyInteger('stamina_attr')->nullable();
            $table->tinyInteger('balance_attr')->nullable();
            $table->tinyInteger('speed_attr')->nullable();
            $table->tinyInteger('jumping_attr')->nullable();
            $table->tinyInteger('aggression_attr')->nullable();
            $table->tinyInteger('composure_attr')->nullable();
            $table->tinyInteger('goal_keeper_attr')->nullable();
            $table->tinyInteger('reaction_attr')->nullable();
            $table->tinyInteger('attack_position_attr')->nullable();

            /* Relationships */
            $table->bigInteger('team_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player');
    }
}
