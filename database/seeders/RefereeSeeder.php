<?php

namespace Database\Seeders;

use App\Models\Referee;
use Illuminate\Database\Seeder;

class RefereeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path()."/json/referee.json";
        $data = json_decode(file_get_contents($path), true);
        foreach ($data as $datum) {
            $referee = new Referee();
            $referee->name = $datum['name'];
            $referee->yellow_card_percent = $datum['yellow_card_percent'];
            $referee->red_card_percent = $datum['red_card_percent'];
            $referee->penalty_percent = $datum['penalty_percent'];
            $referee->save();
        }
    }
}
