<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>PL Simulation</title>
</head>
<body>
<div class="container bg-primary pt-3">
    <div class="row bg-white py-3">
        <div class="col-12 text-center text-muted text-uppercase">Premier League Simulation</div>
    </div>
    <div class="row bg-white">
        <div class="col-12">
            <div class="row py-3">
                <div class="col-12 col-lg-6 mx-auto">
                    <table class="table table-bordered">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">Teams</th>
                            <th scope="col">Overall</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($teams as $team)
                            <tr>
                                <td>{{ $team->name }}</td>
                                @php
                                    $color = 'text-warning';
                                    if($team->overall >= 80) {
                                        $color = 'text-success';
                                    }
                                    if ($team->overall < 75) {
                                        $color = 'text-danger';
                                    }
                                @endphp

                                <td class="{{ $color }}">{{ $team->overall }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <a href="{{ route('fixture.create') }}" class="btn btn-primary w-100 py-3 mt-3">Create Fixture</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

