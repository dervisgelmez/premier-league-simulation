@if(count($teams))
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"></th>
                    <th scope="col">W</th>
                    <th scope="col">D</th>
                    <th scope="col">L</th>
                    <th scope="col">GD</th>
                    <th scope="col">P</th>
                </tr>
                </thead>
                <tbody>
                @foreach($teams as $key => $team)
                    <tr>
                        <th scope="row">{{($key+1)}}</th>
                        <td>{{ $team->name }}</td>
                        <td>{{ $team->win ?? 0 }}</td>
                        <td>{{ $team->draw ?? 0 }}</td>
                        <td>{{ $team->lose ?? 0 }}</td>
                        <td>{{ $team->average }}</td>
                        <td>{{ $team->point }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@else
    <h3>Teams not found.</h3>
@endif
