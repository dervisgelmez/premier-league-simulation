<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>PL Simulation</title>
</head>
<body>
    <div class="container bg-primary pt-3">
        <div class="row bg-white py-3">
            <div class="col-12 text-center text-muted text-uppercase">Premier League Simulation</div>
        </div>
        <div class="row bg-white">
            <div class="col-12 col-md-4">
                @include('navigator')
            </div>
            <div class="col-12 col-md-8">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>
