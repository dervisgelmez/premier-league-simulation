@extends('base')
@section('content')
    <div class="row py-3">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @for ($i = 1; $i <= count($fixtures); $i++)
                    <li class="nav-item" role="presentation">
                        <button class="nav-link @if($i==1) active @endif"
                                data-bs-toggle="tab" data-bs-target="#week{{$i}}"
                                type="button" role="tab">
                            Week {{ $i }}
                        </button>
                    </li>
                @endfor
            </ul>
            <div class="tab-content pb-5">
                @foreach($fixtures as $i => $games)
                    <div class="tab-pane py-4 @if($loop->first)show active @endif" id="week{{$i}}" role="tabpanel">
                        <div class="row">
                        @foreach($games as $game)
                            <div class="col-4">
                                <table class="table table-gray border table-striped">
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <a href="{{ route('team.profile', ['team' => $game->homeTeam->id ])  }}" class="btn btn-sm">{{ $game->homeTeam->name }}</a>
                                        </th>
                                        <td class="text-center">{{ $game->home_team_goals ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <a href="{{ route('team.profile', ['team' => $game->awayTeam->id ])  }}" class="btn btn-sm">{{ $game->awayTeam->name }}</a>
                                        </th>
                                        <td class="text-center">{{ $game->away_team_goals ??  '-' }}</td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="table-primary">
                                        <tr>
                                            <td class="text-center" colspan="2">
                                                @if(!$game->completed)
                                                    <a href="{{ route('simulation.game', ['game' => $game->id]) }}"
                                                       class="btn btn-sm btn-outline-primary">Play</a>
                                                @else
                                                    <small class="text-muted">{{ $game->match_date }}</small>
                                                @endif
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        @endforeach
                        @if(count($games))
                            <hr>
                            <div class="col-12">
                                <a href="{{ route('simulation.weekNumber', ['weekNumber' => $i]) }}"
                                   class="btn btn-sm w-100 btn-primary py-2 mt-2">Play This Week</a>
                                <a href="{{ route('simulation.all') }}"
                                   class="btn btn-sm w-100 btn-primary py-2 mt-2">Play All Week</a>
                            </div>
                        @endif
                        </div>
                    </div>
                @endforeach
                <div class="col-12">
                    <a href="{{ route('fixture.clear') }}"
                       class="btn btn-sm w-100 text-danger">Reset Data</a>
                </div>
            </div>
        </div>
    </div>
@endsection
