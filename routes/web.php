<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\FixtureController;
use App\Http\Controllers\SimulationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class, 'index']);

Route::get('/fixture', [FixtureController::class, 'index'])->name('fixture.index');
Route::get('/fixture/create', [FixtureController::class, 'create'])->name('fixture.create');
Route::get('/fixture/clear', [FixtureController::class, 'clear'])->name('fixture.clear');

Route::get('/team/{team}', [SimulationController::class, 'index'])->name('team.profile');

Route::get('/simulation', [SimulationController::class, 'all'])->name('simulation.all');
Route::get('/simulation/{game}', [SimulationController::class, 'game'])->name('simulation.game');
Route::get('/simulation/week/{weekNumber}', [SimulationController::class, 'week'])->name('simulation.weekNumber');
