<p align="center">
    <a href="https://laravel.com/docs/8.x" target="_blank">
        <img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400">
    </a>
</p>

## Kurulum

- `$ composer install`
- .env dosyasının oluşturulması

- `$ php artisan setup:simulation`
- Takım ve oyuncu verilerini çekilmesi