<?php

namespace App\Http\Service;

use App\Models\Game;
use App\Models\GameMoment;
use App\Models\Player;
use App\Models\Team;

class SimulationService
{
    const attackFunctions = [
        'pass',
        'shoot',
        'longPass',
        'shortPass',
        'dribbling',
        'ballControl'
    ];

    const defenseFunctions = [
        'crossingPass',
        'sideTickle',
        'standTickle',
        'marking'
    ];

    /** @var FixtureService */
    private $fixtureService;
    /** @var Game */
    private $processGame;
    /** @var Team */
    private $homeTeam;
    /** @var Team */
    private $awayTeam;
    /** @var string */
    private $minute;
    /** @var int */
    private $fieldX = 110;
    /** @var int */
    private $fieldY = 90;
    /** @var integer */
    private $ballX;
    /** @var integer */
    private $ballY;
    /** @var Player[] */
    private $players;
    /** @var Player */
    private $attackPlayer;
    /** @var Player */
    private $defensePlayer;
    /** @var integer */
    private $homeTeamGoal;
    /** @var integer */
    private $awayTeamGoal;

    public function __construct(FixtureService $fixtureService)
    {
        $this->fixtureService = $fixtureService;
    }

    public function simulate(Game $game)
    {
        $this->processGame = $game;
        $this->homeTeam = $game->homeTeam;
        $this->awayTeam = $game->awayTeam;
        $this->minute = 1;

        $this->homeTeamGoal = rand(0,6);
        $this->awayTeamGoal = rand(0,6);
        return $this->endGame();

        $this->players = [];
        $this->players = array_merge($this->players, $this->homeTeam->squadPlayers()->all());
        $this->players = array_merge($this->players, $this->awayTeam->squadPlayers()->all());

        $startingTeam = (rand(0,1)) ? $this->homeTeam:$this->awayTeam;
        $this->startGame($startingTeam->name);

        $minute = 90;
        for($this->minute = 1; $this->minute <= $minute; $this->minute++) {
            if ($this->minute == ($minute/2)) {
                $this->halfGame();
            }

            $attackFunction = $this->getFunction(self::attackFunctions);
            if ($attackFunction) {
                $defenseFunction = $this->getFunction(self::defenseFunctions);
                $this->compare($attackFunction, $defenseFunction);
            }

        }
        $this->endGame();
    }

    public function compare(string $attackFunction, string $defenseFunction)
    {


        dd($attackFunction, $defenseFunction);
    }

    public function getFunction(array $functions, bool $required = false)
    {
        $continue = ($required) ? 1 : rand(-3,1);
        if ($continue) {
            return $functions[array_rand($functions)];
        }
        return null;
    }

    public function simulateWeek($weekNumber)
    {
        $games = Game::query()->where('week_number', $weekNumber)->where('completed', null)->get();
        foreach ($games as $game) {
            $this->simulate($game);
        }
    }

    public function simulateAll()
    {
        $games = $this->fixtureService->getActiveFixtureWithGames();
        foreach ($games as $game) {
            $this->simulate($game);
        }
    }

    private function choiceAttack()
    {

    }

    private function choiceDefense()
    {

    }

    private function startGame(string $teamName)
    {
        $data = "The match begins with {$teamName}'s kick";
        $this->setBallPosition(($this->fieldX/2), ($this->fieldY/2));

        $moment = new GameMoment();
        $moment->setAttribute('type', 'starting_game');
        $moment->setAttribute('minute', $this->minute);
        $moment->setAttribute('data', $data);
        $moment->setAttribute('game_id', $this->processGame->id);
        $moment->save();
    }

    private function halfGame()
    {
        $moment = new GameMoment();
        $moment->setAttribute('type', 'starting_game');
        $moment->setAttribute('minute', $this->minute);
        $moment->setAttribute('data', 'We continue where we left off in the second half.');
        $moment->setAttribute('game_id', $this->processGame->id);
        $moment->save();
    }

    private function endGame()
    {
        $this->processGame->setAttribute('completed', true);
        $this->processGame->setAttribute('home_team_goals', $this->homeTeamGoal);
        $this->processGame->setAttribute('away_team_goals', $this->awayTeamGoal);
        $this->processGame->save();

        if ($this->homeTeamGoal > $this->awayTeamGoal) {
            $this->win();
        }
        if ($this->homeTeamGoal == $this->awayTeamGoal) {
            $this->draw();
        }
        if ($this->homeTeamGoal < $this->awayTeamGoal) {
            $this->lose();
        }
    }

    private function win()
    {
        $goalDiff = ($this->homeTeamGoal-$this->awayTeamGoal);

        $winCount = $this->homeTeam->getAttribute('win');
        $point = $this->homeTeam->getAttribute('point');
        $average = $this->homeTeam->getAttribute('average');

        $this->homeTeam->setAttribute('win', ($winCount+1));
        $this->homeTeam->setAttribute('point', ($point+3));
        $this->homeTeam->setAttribute('average', ($average+$goalDiff));
        $this->homeTeam->save();

        $loseCount = $this->awayTeam->getAttribute('lose');
        $this->awayTeam->setAttribute('lose', ($loseCount+1));
        $this->awayTeam->setAttribute('average', ($average-$goalDiff));
        $this->awayTeam->save();
    }

    private function draw()
    {
        $drawCount = $this->homeTeam->getAttribute('draw');
        $point = $this->homeTeam->getAttribute('point');
        $this->homeTeam->setAttribute('draw', ($drawCount+1));
        $this->homeTeam->setAttribute('point', ($point+1));
        $this->homeTeam->save();

        $drawCount = $this->awayTeam->getAttribute('draw');
        $point = $this->awayTeam->getAttribute('point');
        $this->awayTeam->setAttribute('draw', ($drawCount+1));
        $this->homeTeam->setAttribute('point', ($point+1));
        $this->awayTeam->save();
    }

    private function lose()
    {
        $goalDiff = ($this->awayTeamGoal-$this->homeTeamGoal);

        $winCount = $this->awayTeam->getAttribute('win');
        $point = $this->awayTeam->getAttribute('point');
        $average = $this->awayTeam->getAttribute('average');

        $this->awayTeam->setAttribute('win', ($winCount+1));
        $this->awayTeam->setAttribute('point', ($point+3));
        $this->awayTeam->setAttribute('average', ($average+$goalDiff));
        $this->awayTeam->save();

        $loseCount = $this->homeTeam->getAttribute('lose');
        $this->homeTeam->setAttribute('lose', ($loseCount+1));
        $this->homeTeam->setAttribute('average', ($average-$goalDiff));
        $this->homeTeam->save();
    }

    private function setBallPosition($x, $y)
    {
        $this->ballX = $x;
        $this->ballY = $y;
    }
}
