<?php

namespace App\Http\Service;

use App\Http\Type\PlayerType;
use App\Http\Type\TeamType;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class DataService
{
    private $root = 'https://www.fifaindex.com';
    const teamsUrl = '/teams/?league=13&order=desc';

    public function __construct()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
    }

    /**
     * @param string $url
     * @return object|Crawler
     */
    private function getCrawler(string $url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "{$this->root}{$url}");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
            curl_setopt($ch, CURLOPT_TIMEOUT,30);
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

            $content = curl_exec($ch);
            if ($content === false || is_null($content)) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            $crawler = new Crawler($content);
            return $crawler->filter('body')->children('main > .container > .row > .col-lg-8');

        } catch(Exception $e) {
            trigger_error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
        } finally {
            if (is_resource($ch)) {
                curl_close($ch);
            }
        }
    }

    /**
     * @return TeamType[]|array
     */
    public function getTeams(): array
    {
        $teams = [];
        $this->getCrawler(self::teamsUrl)
            ->children('.responsive-table > table > tbody')
            ->filter('tr')->each(function (Crawler $node) use (&$teams) {
                $teamRow = $node->filter('td[data-title="Name"]');
                if ($teamRow->count()) {
                    $team = new TeamType();
                    $team->name = $node->filter('td[data-title="Name"]')->text();
                    $team->attack = $node->filter('td[data-title="ATT"]')->text();
                    $team->middle = $node->filter('td[data-title="MID"]')->text();
                    $team->defense = $node->filter('td[data-title="DEF"]')->text();
                    $team->overall = $node->filter('td[data-title="OVR"]')->text();

                    $teamLink = $node->filter('td[data-title="Name"] a.link-team')->attr('href');
                    $team->players = $this->getPlayers($teamLink);

                    $teams[] = $team;
                }
            });

        return $teams;
    }

    private function getPlayers(string $url): array
    {
        $teamCrawler = $this->getCrawler($url);

        $lineUp = [];
        $teamCrawler
            ->filter('.row')
            ->eq(1)
            ->children('div.col-md-8 > div.card > div.card-body > div.formation')
            ->filter('div.player')
            ->each(function (Crawler $node) use (&$lineUp) {
                $lineUp[] = $node->filter('a.link-player')->attr('href');
            });

        $players = [];
        $teamCrawler
            ->filter('div.responsive-table')
            ->eq(0)
            ->children('table.table-players > tbody')
            ->filter('tr')
            ->each(function (Crawler $node) use (&$players, &$lineUp) {
                $playerRow = $node->filter('td[data-title="Name"]');
                if ($playerRow->count()) {
                    $player = new PlayerType();
                    $player->name = $playerRow->text();
                    $player->number = $node->filter('td[data-title="Kit Number"]')->text();
                    $player->position = $node->filter('td[data-title="Position"]')->text();
                    $player->overall = $node->filter('td[data-title="OVR / POT"] span')->eq(0)->text();
                    $player->link = $playerRow->filter('a')->attr('href');
                    $player->squad = in_array($player->link, $lineUp);
                    $player = $this->getPlayerAttributes($player);

                    $players[] = $player;
                }
            });

        return $players;
    }

    private function getPlayerAttributes(PlayerType $player): PlayerType
    {
        $playerCrawler = $this->getCrawler($player->link);

        $playerInfo = $playerCrawler->filter('.row')->eq(0)->filter('.col-sm-6')->eq(1)->filter('.card');
        $playerInfoList = $playerInfo->filter('.card-body p');
        $player->age = $playerInfoList->eq(4)->filter('span')->text();
        $player->foot = $playerInfoList->eq(2)->filter('span')->text();

        $playerAttributes = $playerCrawler->filter('.masonry .item');
        $playerBallSkills = $playerAttributes->eq(0)->filter('.card .card-body p');
        $playerDefence = $playerAttributes->eq(1)->filter('.card .card-body p');
        $playerMental = $playerAttributes->eq(2)->filter('.card .card-body p');
        $playerPassing = $playerAttributes->eq(3)->filter('.card .card-body p');
        $playerPhysical = $playerAttributes->eq(4)->filter('.card .card-body p');
        $playerShooting = $playerAttributes->eq(5)->filter('.card .card-body p');
        $playerGoalkeeper = $playerAttributes->eq(6)->filter('.card .card-body p');

        $player->ballControlAttr = $playerBallSkills->eq(0)->filter('span')->text();
        $player->dribblingAttr = $playerBallSkills->eq(1)->filter('span')->text();
        $player->markingAttr = $playerDefence->eq(0)->filter('span')->text();
        $player->slideTackleAttr = $playerDefence->eq(1)->filter('span')->text();
        $player->standTackleAttr = $playerDefence->eq(2)->filter('span')->text();
        $player->aggressionAttr = $playerMental->eq(0)->filter('span')->text();
        $player->reactionAttr = $playerMental->eq(1)->filter('span')->text();
        $player->attackPositionAttr = $playerMental->eq(2)->filter('span')->text();
        $player->composureAttr = $playerMental->eq(5)->filter('span')->text();
        $player->crossingPassAttr = $playerPassing->eq(0)->filter('span')->text();
        $player->shortPassAttr = $playerPassing->eq(1)->filter('span')->text();
        $player->longPassAttr = $playerPassing->eq(2)->filter('span')->text();
        $player->accelerationAttr = $playerPhysical->eq(0)->filter('span')->text();
        $player->staminaAttr = $playerPhysical->eq(1)->filter('span')->text();
        $player->strengthAttr = $playerPhysical->eq(2)->filter('span')->text();
        $player->balanceAttr = $playerPhysical->eq(3)->filter('span')->text();
        $player->speedAttr = $playerPhysical->eq(4)->filter('span')->text();
        $player->jumpingAttr = $playerPhysical->eq(6)->filter('span')->text();
        $player->headShotAttr = $playerShooting->eq(0)->filter('span')->text();
        $player->powerShotAttr = $playerShooting->eq(1)->filter('span')->text();
        $player->finishingAttr = $playerShooting->eq(2)->filter('span')->text();
        $player->longShotAttr = $playerShooting->eq(3)->filter('span')->text();
        $player->curveShootAttr = $playerShooting->eq(4)->filter('span')->text();
        $player->freeKickAttr = $playerShooting->eq(5)->filter('span')->text();
        $player->penaltyAttr = $playerShooting->eq(6)->filter('span')->text();
        $player->goalKeeperAttr = $playerGoalkeeper->eq(3)->filter('span')->text();

        return $player;
    }
}
