<?php

namespace App\Http\Service;

use App\Models\Team;

class TeamService
{
    public function getTeams()
    {
        return Team::query()->orderBy('name', 'ASC')->get();
    }

    public function getTeamsStandings()
    {
        return Team::query()->orderByDesc('point')->orderByDesc('average')->orderBy('name')->get();
    }

    public function getTeamCount(): int
    {
        return Team::query()->count();
    }
}
