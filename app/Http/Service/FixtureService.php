<?php

namespace App\Http\Service;

use App\Http\Type\FixtureType;
use App\Http\Type\GameType;
use App\Models\Fixture;
use App\Models\Game;
use App\Models\Referee;

class FixtureService
{
    /**
     * @var TeamService
     */
    private $teamService;

    public function __construct(
        TeamService $teamService
    )
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $this->teamService = $teamService;
    }

    public function getActiveFixture()
    {
        return Fixture::query()->where(['deleted_at' => null])->orderBy('id', 'DESC')->first();
    }

    public function getActiveFixtureWithGamesAndWeek(): array
    {
        $games = [];
        $fixture = $this->getActiveFixture();
        if ($fixture) {
            $gameModels = Game::query()->where(['fixture_id' => $fixture->getAttribute('id')])->get();
            foreach ($gameModels as $gameModel) {
                $games[$gameModel->week_number][] = $gameModel;
            }
        }

        return $games;
    }

    public function getActiveFixtureWithGames(): array
    {
        $games = [];
        $fixture = $this->getActiveFixture();
        if ($fixture) {
            $gameModels = Game::query()->where(['fixture_id' => $fixture->getAttribute('id')])->get();
            foreach ($gameModels as $gameModel) {
                $games[] = $gameModel;
            }
        }

        return $games;
    }

    private function generateGames(): array
    {
        $games = [];
        $teams = $this->teamService->getTeams();
        foreach ($teams as $iTeam) {
            foreach ($teams as $jTeam) {
                if ($iTeam !== $jTeam) {
                    $gameType = new GameType();
                    $gameType->homeTeam = $iTeam;
                    $gameType->awayTeam = $jTeam;
                    $games[] = $gameType;
                }
            }
        }

        return $games;
    }

    private function generateFixtures(): array
    {
        $fixtures = [];
        $games = $this->generateGames();
        $referee = Referee::all()->modelKeys();

        $teamCount = $this->teamService->getTeamCount();
        $weekCount = 5;
        $weeklyGameCount = $teamCount/2;

        $startDate = new \DateTime();
        $startDate->setDate( date('Y'), 8, 13);
        for ($i = 1; $i <= $weekCount; $i++) {
            $selectedTeam = [];
            $selectedReferee = [];
            /** @var GameType $game */
            for ($j = 1; $j <= $weeklyGameCount; $j++) {
                while (true) {
                    $randomGameKey = array_rand($games);
                    $game = $games[$randomGameKey];
                    $homeTeamKey = $game->homeTeam->id;
                    $awayTeamKey = $game->awayTeam->id;

                    if (!isset($fixtures[$randomGameKey]) && !isset($selectedTeam[$homeTeamKey]) && !isset($selectedTeam[$awayTeamKey])) {
                        break;
                    }
                }

                while (true) {
                    $refereeRandomKey = array_rand($referee);
                    $game->referee = $referee[$refereeRandomKey];
                    if (!isset($selectedReferee[$refereeRandomKey])) {
                        break;
                    }
                }

                $gameDate = clone $startDate;
                $gameDate->modify(rand(0, 4).' day');
                $game->date = $gameDate;

                $fixture = new FixtureType();
                $fixture->week = $i;
                $fixture->gameType = $game;
                $fixtures[$randomGameKey] = $fixture;

                $selectedTeam[$homeTeamKey] = true;
                $selectedTeam[$awayTeamKey] = true;
                $selectedReferee[$refereeRandomKey] = true;

                unset($games[$randomGameKey]);
                $games = array_values($games);

                $startDate->modify('+1 week');
            }
        }

        return $fixtures;
    }

    public function create(): Fixture
    {
        $fixtures = $this->generateFixtures();

        $this->clearAllFixtures();
        $fixture = new Fixture();
        $fixture->setAttribute('name', 'Premier League Fixture');
        $fixture->save();

        /** @var FixtureType $fixtureType */
        foreach ($fixtures as $fixtureType) {
            $game = new Game();
            $game->setAttribute('week_number', $fixtureType->week);
            $game->setAttribute('match_date', $fixtureType->gameType->date);
            $game->setAttribute('home_team_id', $fixtureType->gameType->homeTeam->getAttribute('id'));
            $game->setAttribute('away_team_id', $fixtureType->gameType->awayTeam->getAttribute('id'));
            $game->setAttribute('fixture_id', $fixture->getAttribute('id'));
            $game->setAttribute('referee_id', $fixtureType->gameType->referee);
            $game->save();
        }

        return $fixture;
    }

    public function clearAllFixtures()
    {
        Fixture::query()->where(['deleted_at' => null])->update(['deleted_at' => new \DateTime()]);
    }
}
