<?php

namespace App\Http\Controllers;

use App\Http\Service\SimulationService;
use App\Models\Game;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;

class SimulationController extends BaseController
{
    public function game(Game $game, SimulationService $simulationService): RedirectResponse
    {
        $simulationService->simulate($game);

        return redirect()->action([FixtureController::class, 'index']);
    }

    public function week(string $weekNumber, SimulationService $simulationService): RedirectResponse
    {
        $simulationService->simulateWeek($weekNumber);

        return redirect()->action([FixtureController::class, 'index']);
    }

    public function all(SimulationService $simulationService): RedirectResponse
    {
        $simulationService->simulateAll();

        return redirect()->action([FixtureController::class, 'index']);
    }
}
