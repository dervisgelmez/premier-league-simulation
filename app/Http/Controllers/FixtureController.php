<?php

namespace App\Http\Controllers;

use App\Http\Service\FixtureService;
use App\Http\Service\TeamService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;

class FixtureController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(FixtureService $fixtureService, TeamService $teamService)
    {
        $fixtures = $fixtureService->getActiveFixtureWithGamesAndWeek();
        if (empty($fixtures)) {
            return redirect()->action([Controller::class, 'index']);
        }

        return view('fixture/index', [
            'fixtures' => $fixtures,
            'teams' => $teamService->getTeamsStandings()
        ]);
    }

    public function create(FixtureService $fixtureService): RedirectResponse
    {
        $fixtureService->create();

        return redirect()->action([FixtureController::class, 'index']);
    }

    public function clear(FixtureService $fixtureService): RedirectResponse
    {
        $fixtureService->clearAllFixtures();

        return redirect()->action([Controller::class, 'index']);
    }
}
