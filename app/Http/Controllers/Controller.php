<?php

namespace App\Http\Controllers;

use App\Http\Service\FixtureService;
use App\Http\Service\TeamService;
use App\Models\Fixture;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(TeamService $teamService, FixtureService $fixtureService)
    {
        $fixture = $fixtureService->getActiveFixture();
        if ($fixture instanceof Fixture) {
            return redirect()->action([FixtureController::class, 'index']);
        }

        return view('index', [
            'teams' => $teamService->getTeams()
        ]);
    }
}
