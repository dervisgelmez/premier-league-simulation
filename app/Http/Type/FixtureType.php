<?php

namespace App\Http\Type;

class FixtureType
{
    /** @var integer */
    public $week;

    /** @var GameType */
    public $gameType;
}
