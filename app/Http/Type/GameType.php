<?php

namespace App\Http\Type;

use App\Models\Referee;
use App\Models\Team;

class GameType
{
    /** @var Team */
    public $homeTeam;

    /** @var Team */
    public $awayTeam;

    /** @var \DateTime */
    public $date;

    /** @var Referee */
    public $referee;
}
