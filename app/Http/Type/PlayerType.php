<?php

namespace App\Http\Type;

class PlayerType
{
    public $name;

    public $link;

    public $number;

    public $position;

    public $overall;

    public $age;

    public $foot;

    public $squad;

    public $ballControlAttr;

    public $dribblingAttr;

    public $crossingPassAttr;

    public $shortPassAttr;

    public $longPassAttr;

    public $powerShotAttr;

    public $finishingAttr;

    public $longShotAttr;

    public $headShotAttr;

    public $curveShootAttr;

    public $freeKickAttr;

    public $penaltyAttr;

    public $slideTackleAttr;

    public $standTackleAttr;

    public $markingAttr;

    public $accelerationAttr;

    public $strengthAttr;

    public $staminaAttr;

    public $balanceAttr;

    public $speedAttr;

    public $jumpingAttr;

    public $aggressionAttr;

    public $composureAttr;

    public $goalKeeperAttr;

    public $reactionAttr;

    public $attackPositionAttr;
}
