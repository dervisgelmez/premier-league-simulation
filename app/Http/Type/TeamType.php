<?php

namespace App\Http\Type;

class TeamType
{
    /** @var string */
    public $name;

    /** @var integer */
    public $attack;

    /** @var integer */
    public $middle;

    /** @var integer */
    public $defense;

    /** @var integer */
    public $overall;

    /** @var PlayerType[] */
    public $players;
}
