<?php

namespace App\Http\Type;

use App\Models\Team;

class GameStatisticsType
{
    /** @var Team */
    public $type;

    /** @var Team */
    public $homeTeamValue;

    /** @var \DateTime */
    public $awayTeamValue;
}
