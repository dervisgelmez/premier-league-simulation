<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Player extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player';

    protected $fillable = [
        'name',
        'number',
        'position',
        'age',
        'foot',
        'squad',
        'goal',
        'assist',
        'red_card',
        'yellow_card',
        'injury_week',
        'out_of_play',
        'overall',
        'ball_control_attr',
        'dribbling_attr',
        'crossing_pass_attr',
        'short_pass_attr',
        'long_pass_attr',
        'power_shot_attr',
        'finishing_attr',
        'long_shot_attr',
        'head_shot_attr',
        'curve_shoot_attr',
        'free_kick_attr',
        'penalty_attr',
        'slide_tackle_attr',
        'stand_tackle_attr',
        'marking_attr',
        'acceleration_attr',
        'strength_attr',
        'stamina_attr',
        'balance_attr',
        'speed_attr',
        'jumping_attr',
        'aggression_attr',
        'composure_attr',
        'goal_keeper_attr',
        'reaction_attr',
        'attack_position_attr',
        'team_id'
    ];

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }
}
