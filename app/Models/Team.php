<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Team extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team';

    protected $fillable = [
        'name',
        'attack',
        'middle',
        'defense',
        'overall'
    ];

    public function games()
    {
        return $this->hasMany(Game::class);
    }

    public function players(): Collection
    {
        return $this->hasMany(Player::class)->get();
    }

    public function squadPlayers(): Collection
    {
        return $this->hasMany(Player::class)->where('squad', true)->get();
    }
}
