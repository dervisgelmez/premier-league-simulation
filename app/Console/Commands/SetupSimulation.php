<?php

namespace App\Console\Commands;

use App\Http\Service\DataService;
use App\Models\Player;
use App\Models\Team;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class SetupSimulation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:simulation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var DataService
     */
    private $dataService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DataService $dataService)
    {
        parent::__construct();
        $this->dataService = $dataService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->createDatabase();
        $this->info('Database created. 🔌');
        $this->line('-------------------');

        DB::reconnect('mysql');

        Artisan::call('migrate');
        Artisan::call('db:seed --class=RefereeSeeder');
        $this->info('Database updated. 🔌');
        $this->line('-------------------');


        $this->line('');
        $this->info("Now... I'm looking for some data to make everything work better... 👨‍💻");
        sleep(2);
        $this->line('This process may take a while 🥺');
        $this->line('');

        $teamTypes = $this->dataService->getTeams();
        $this->line('Last few things left. 🙏');
        $this->line('');
        $bar = $this->output->createProgressBar(count($teamTypes));
        $bar->start();

        foreach ($teamTypes as $teamType) {
            $team = new Team();
            $team->name = $teamType->name;
            $team->attack = $teamType->attack;
            $team->middle = $teamType->middle;
            $team->defense = $teamType->defense;
            $team->overall = $teamType->overall;
            $team->average = 0;
            $team->point = 0;
            $team->save();
            foreach ($teamType->players as $playerType) {
                $player = new Player();
                $player->name = $playerType->name;
                $player->number = $playerType->number;
                $player->position = $playerType->position;
                $player->age = $playerType->age;
                $player->foot = $playerType->foot;
                $player->squad = $playerType->squad;
                $player->overall = $playerType->overall;
                $player->ball_control_attr = $playerType->ballControlAttr;
                $player->dribbling_attr = $playerType->dribblingAttr;
                $player->crossing_pass_attr = $playerType->crossingPassAttr;
                $player->short_pass_attr = $playerType->shortPassAttr;
                $player->long_pass_attr = $playerType->longPassAttr;
                $player->power_shot_attr = $playerType->powerShotAttr;
                $player->finishing_attr = $playerType->finishingAttr;
                $player->long_shot_attr = $playerType->longShotAttr;
                $player->head_shot_attr = $playerType->headShotAttr;
                $player->curve_shoot_attr = $playerType->curveShootAttr;
                $player->free_kick_attr = $playerType->freeKickAttr;
                $player->penalty_attr = $playerType->penaltyAttr;
                $player->slide_tackle_attr = $playerType->slideTackleAttr;
                $player->stand_tackle_attr = $playerType->standTackleAttr;
                $player->marking_attr = $playerType->markingAttr;
                $player->acceleration_attr = $playerType->accelerationAttr;
                $player->strength_attr = $playerType->strengthAttr;
                $player->stamina_attr = $playerType->staminaAttr;
                $player->balance_attr = $playerType->balanceAttr;
                $player->speed_attr = $playerType->speedAttr;
                $player->jumping_attr = $playerType->jumpingAttr;
                $player->aggression_attr = $playerType->aggressionAttr;
                $player->composure_attr = $playerType->composureAttr;
                $player->goal_keeper_attr = $playerType->goalKeeperAttr;
                $player->reaction_attr = $playerType->reactionAttr;
                $player->attack_position_attr = $playerType->attackPositionAttr;
                $player->team_id = $team->id;
                $player->save();
            }
            $bar->advance();
        }
        $bar->finish();

        $this->line('');
        $this->line('-----------------------------------------');
        $this->info('Thanks for waiting, everything is OK. 😎');

        return Command::SUCCESS;
    }

    private function createDatabase()
    {
        $schemaName = env('DB_DATABASE');
        $charset = env('DB_CHARSET');
        $collation = env('DB_COLLATION');
        config(["database.connections.mysql.database" => null]);

        $query = "CREATE DATABASE IF NOT EXISTS $schemaName CHARACTER SET $charset COLLATE $collation;";
        DB::statement($query);

        config(["database.connections.mysql.database" => $schemaName]);
    }
}
